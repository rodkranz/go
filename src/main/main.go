package main

import (
	"fmt"
	"flag"
	"pkg/util"
)

func main() {

	quiet := flag.Bool("q", false, "Execute quiet")
	wordPtr := flag.String("r", "", "Neet to be a string *")

	flag.Parse()

	if len(*wordPtr) == 0 {
		fmt.Println( "The [-r] param cannot be empty!" )
	} else {
		if *quiet == true {
			fmt.Println( util.Reverse(*wordPtr) )
		} else {
			fmt.Println("result:", util.Reverse(*wordPtr))
		}
	}

}
